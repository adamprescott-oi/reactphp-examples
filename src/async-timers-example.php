<?php
require __DIR__ . '/../vendor/autoload.php';

$loop = \React\EventLoop\Factory::create();

$timer = $loop->addPeriodicTimer(0.3, function() {
    echo 'hello';
});

$loop->addTimer(2.0, function () use ($loop, $timer) {
    $loop->cancelTimer($timer);
});


$loop->run();