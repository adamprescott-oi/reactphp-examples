<?php
require __DIR__ . '/../vendor/autoload.php';

$loop = \React\EventLoop\Factory::create();

$browser = new \Clue\React\Buzz\Browser($loop);

//$browser->get('http://api.twitter.com/')->then(
//    function (\Psr\Http\Message\ResponseInterface $response) {
//    var_dump($response->getHeaders());
//    },
//    function (Exception $error) {
//        echo $error;
//        echo 'HELLO';
//    }
//);

//$browser->get('http://api.github.com/')->then(function (\Psr\Http\Message\ResponseInterface $response) {
//    echo $response->getBody();
//});

$packagist = new Clue\React\Packagist\Api\Client($browser);
$packagist->get('react/stream')->then(function ($info) {
    var_dump($info);
});

$loop->run();