<?php
require __DIR__ . '/../vendor/autoload.php';

$loop = \React\EventLoop\Factory::create();

$server = new \React\Socket\Server(8080, $loop);
$server->on('connection', function (\React\Socket\ConnectionInterface $conn) use ($server) {
//    $conn->on('data', function($data) use ($conn) {
//        $conn->write($data);
//    });

    $conn->on('data', function($data) use ($conn) {
        $conn->end($data);
    });

//    $conn->pipe($conn);

//    $server->close();
});

$resource = stream_socket_client('127.0.0.1:8080'); // Blocking
$http = new \React\Stream\DuplexResourceStream($resource, $loop);
$http->write("GET / HTTP/1.0\r\n\r\n");

$http->on('data', function ($data) {
    echo $data;
});

$http->on('close', function () {
    echo 'closed';
});

$loop->run();